import requests

class Client():
    def __init__(self, config):
        self.config = config
        self.all_users = self.get_all_users()


    def url_base(self):
        if self.config.get("sandbox", False):
            return f"https://api.travefy.com:81/api/v2"
        else:
            return f"https://api.travefy.com/api/v2"


    def get_headers(self):
        headers = {}
        headers["X-API-PRIVATE-KEY"] = self.config["private_key"]
        headers["X-API-PUBLIC-KEY"] = self.config["public_key"]
        return headers


    def post(self, endpoint, payload):
        headers = self.get_headers()
        headers["Content-Type"] = "application/json"
        url = f"{self.url_base()}/{endpoint}"
        response = requests.post(url, data=payload, headers=headers)        
        return response


    def put(self, endpoint, payload, id):
        headers = self.get_headers()
        headers["Content-Type"] = "application/json"
        url = f"{self.url_base()}/{endpoint}/{id}"
        response = requests.put(url, data=payload, headers=headers)
        return response


    def get_all_users(self):
        headers = self.get_headers()    
        url = f"{self.url_base()}/users/"
        result = requests.get(url, headers=headers)
        result = result.json()
        users = {}

        for user in result:
            users[user['User']['Username']] = user['User']

        return users

