"""Travefy target sink class, which handles writing streams."""


from singer_sdk.sinks import RecordSink
from singer_sdk.plugin_base import PluginBase
from typing import Dict, Optional, List

import json

class TravefySink(RecordSink):
    """Travefy target sink class."""

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(
            target=target,
            stream_name=stream_name,
            schema=schema,
            key_properties=key_properties
        )

        self.client = target.client


    def process_record(self, user: dict, context: dict) -> None:
        """Process the user."""
        all_users = self.client.all_users
        user_json = json.dumps({"User": user})

        if (user['Username'] in all_users):
            # Get id from all users
            current_user = all_users[user['Username']]
            # Put the record
            self.client.put('users', user_json, current_user['Id'])
        else:
            # Post
            self.client.post('users', user_json)    

