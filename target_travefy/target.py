"""Travefy target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from pathlib import PurePath
from typing import List, Optional, Union

from target_travefy.sinks import (
    TravefySink,
)

from target_travefy.client import Client


class TargetTravefy(Target):
    """Sample target for Travefy."""

    name = "target-travefy"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "private_key",
            th.StringType
        ),
        th.Property(
            "public_key",
            th.StringType
        ),
    ).to_dict()
    default_sink_class = TravefySink

    def __init__(
        self,
        config: Optional[Union[dict, PurePath, str, List[Union[PurePath, str]]]] = None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        super().__init__(
            config=config,
            parse_env_config=parse_env_config,
            validate_config=validate_config,
        )

        # Create client
        self.client = Client(self.config)
